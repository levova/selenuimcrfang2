package Firefox;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class test1 {
    private WebDriver driver;
    private WebDriverWait wait;

    @BeforeClass
    public void setStart(){
        System.out.println(getClass());
    }

    @BeforeMethod
    public void setUp() throws InterruptedException {
        FirefoxBinary firefoxBinary = new FirefoxBinary();
        FirefoxOptions options = new FirefoxOptions();
        options.setBinary(firefoxBinary);
        options.setHeadless(false);  // <-- headless set here

        driver = new FirefoxDriver(options);
        wait = new WebDriverWait(driver, 60);

        driver.manage().window().maximize();
        driver.get("https://sandbox.allpointsmessaging.com:15443/crf-web");
    }

    @AfterMethod
    public void tearDown() throws InterruptedException {
        driver.quit();
    }

    @Test
    public void loginScreen() throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("input1")));

        System.out.println("First bitbucket pipeline test");
        driver.findElement(By.id("input1")).sendKeys("qwerty");
        Thread.sleep(2000);

        String s = driver.findElement(By.id("input1")).getText();
        System.out.println(s);

        Assert.assertEquals(1,1);
        Thread.sleep(2000);
    }
}
